﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grab : MonoBehaviour
{
    private bool contact;
    List<GameObject> collisions;
    Vector3 controllerV0, controllerV, objV;

    private void Awake()
    {
        controllerV0 = gameObject.transform.position;
    }


    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();
        if (contact && (OVRInput.Get(OVRInput.RawButton.RHandTrigger)))
        {
            controllerV = gameObject.transform.position - controllerV0;
            foreach(GameObject obj in collisions)
            {
                if(obj.tag == "Grabbable")
                {
                    controllerV = gameObject.transform.position - controllerV0;
                    obj.transform.position = controllerV;
                }
            }
        } 
    }

    private void OnTriggerEnter(Collider other)
    {
        contact = true;
        collisions.Add(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        contact = false;
        collisions.Remove(other.gameObject);
    }
}
